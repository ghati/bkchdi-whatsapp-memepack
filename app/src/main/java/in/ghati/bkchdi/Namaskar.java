package in.ghati.bkchdi;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Namaskar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_namaskar);
    }

    public void hideShame(View v)
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Namaskar.this);

        // Objects for hiding commands

        PackageManager p = getPackageManager();
        ComponentName componentName = new ComponentName(this, in.ghati.bkchdi.Namaskar.class );

        builder1.setMessage("App Icon will no longer be visible in the app drawer. Please keep me installed so that I can collect data and mine bitcoin in the background :)");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Begin Aache Din",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        p.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "Bhak Bsdk",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public void sendStickers(View v)
    {
        Intent intent = new Intent(this, EntryActivity.class);
        startActivity(intent);
    }


}
